## Nuria Oliver (Spain)
- [http://covid19impactsurvey.org]
- Evidence-based policy-making.
### Barriers to using data to make policy.
- No data mindset;
- Lack of capacity.
- Lack of maturity - operationally deficient.
- Uncoordinated.
### Team composition/activities
- Mobile Data Analysis
- Epidemological Models - computational epidemological models (Python, R)
- Additional modelling
### Data
- Mobile data - origin-destination matrices
- Health data
- Survey answers, GIS, Nursing homes
- Lack of data on social contact behaviour, economic and labout impact, prevalence of symptoms, lack of tests.
- See: [http://covid19impactsurvey.org] (launched 28th March) - 24 questions. 
### Questions
#### Mobile
- How has mobility changed?
- How have interventions impacted mobility?
* Health zones
#### Modelling
- How many infected?
- Death modelling
- Risk maps
- ICU/Hospitalisation prediction

## Richard Neher (Biozentrum, University of Basel)
- [neherlab.org/202004_Ellis.html]
### Tracking SARS-Cov-2 using viral genomes and projecting its spread
- Diversity and spread tracking in NextStrain
- Phylogenetic trees of all SARS-Cov-2
- Mutations accumulated 
- Genomes from Iceland - European and US
- Genomes allow us to trace outbreak development - transmission chains and clusters.
- 2500 full genomes
- heavily biased by sampling and sequencing efforts
- approx. one change every 2 weeks - limited temporal resolution.
### Predictions and scenarios
- covid-19 scenarios web app to play with parameters.
- Transport and contact networks can support.
- Issue of underreporting.
### Q&A
- High mutation rate does not necessarily mean difficult to vaccinate against - coronavirus mutation rates tend to be lower than RNA viruses.

## Christopher Mason
- Microbrial monitoring network
- MetaSUB - global network of scientists (http://metasub.org)
- Global K-mer index; multi-sample sequence index.
- Predictive microbial signatures - different cities with different predictive accuracies (RFC).
- RNA virus tracking.
- LAMP reaction - pH - Limit of detection between 10 and 100 copies.
- NextStrain.
- Pop up labs for testing.
- [http://covid19hg.netlify.com]


## Guenter Klaubauer
- Inhibition of the SARS-Cov-2 virus
- Computer-aided drug discovery - virtual screening, compound design (by ML)... 
- Generative models for molecules using VAE, RL, GAN, RNN, and hybrid approaches
- Virtual screening to find already available molecules - can be ligand-based (substructure) or structure-based (docking).
- Molecules can be represented in different ways - as vector of features (FNNs), sequences (RNNs), graphs (Graph NN), Point-based NNs.
- SVM and deep learning perform well (AUC ~0.9).
- SmilesLSTM
- JEDI challenge - molecules against covid-19

## Yoshua Bengino (Mila)
- Feretti et al. Quantifying SARS Cov-2 - contact tracing using phones.
- Digital contact tracing to bring R0 below 1 - can scale beyond manual tracing.
- Not just direct contact - can assess risk based on more global view (e.g. yesterday I met someone who met someone whose husband has covid-19, rather than just yesterday I met someone with covid-19).
- Issue with privacy - app which protects privacy.
- Private set intersection (PSI) method.
- Each phone tries to predict its own posterior probability of being infected (risk level) at different times in the last two weeks based on information available up to now (day resolution).
- Convergence to marginal distribution by Loopy Belief Propagation (run loopy and asynchronously).
- Predictions decomposed into smaller meaning pieces, e.g. transmission probability (given duration/distance of contact), conditional probabilities of symptoms/test results.
- Simulation models can be used to generate data to initialise risk predictor and evaluate ML solutions.

## Mihaela van der Schaar (Cambridge)
- PHE data to help hospials cope.
- ML-AIM Lab - Cambridge Centre for AI in Medicine.
- Support decisions clinicians and managers have to make to save lives.
- Forecast:
  - personalised risk;
  - personalised patient benefit;
  - which treatments neded when by each patient;
  - which resources needed and when by each patient;
  - Future resource requirements (occupancy).
- Collaboration with PHE - CHESS - Hospitalisation in England Surveillance 
  - Personal information
  - Lab details
  - Hospitalisation details
  - Risk factors
  - Outcomes so far
  - (1694 patients; 649 ICU; 307 invasive ventilators)
- AutoProgonosis - predict risk of death
- AUC for predicting mortality - .871 all features with model/vs. .773 with Cox regression (age .799); Age most mportant feature in prediction.
- [https://linkedin.com/pulse/responding-covid-19-ai-machine-learning-mihaela-van-der-schaar]
- Forecasting real time with Adjutorium

## Fred Hamprecht (Data against covid-19)
- [https://www.data-against-covid.org]

## Devret (Kaggle)


